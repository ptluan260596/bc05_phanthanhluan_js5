function gia250Kw(soKw) {
  var tongTien250Kw = 0;
  if (soKw >= 350) {
    tongTien250Kw =
      (soKw - 350) * 1300 + 150 * 1100 + 100 * 850 + 50 * 650 + 50 * 500;
  } else if (soKw >= 200) {
    tongTien250Kw = (soKw - 200) * 1100 + 100 * 850 + 50 * 650 + 50 * 500;
  } else if (soKw >= 100) {
    tongTien250Kw = (soKw - 100) * 850 + 50 * 650 + 50 * 500;
  } else if (soKw >= 50) {
    tongTien250Kw = (soKw - 50) * 650 + 50 * 500;
  } else {
    tongTien250Kw = soKw * 500;
  }
  return tongTien250Kw;
}

function tinhTienDien() {
  var soKw = document.getElementById("soKw").value;
  var ten = document.getElementById("username").value;
  var giaTien = gia250Kw(soKw);
  document.getElementById(
    "tongTien"
  ).innerHTML = `Tổng tiền điện của ${ten} là: ${giaTien.toLocaleString()} đồng`;
}
