function tinhDiemDoiTuong(Dt) {
  if (Dt == "1") {
    return 2.5;
  } else if (Dt == "2") {
    return 1.5;
  } else {
    return 1;
  }
}

function tinhDiemKhuVuc(Kv) {
  if (Kv == "A") {
    return 2;
  } else if (Kv == "B") {
    return 1;
  } else {
    return 0.5;
  }
}

function tinhDiem() {
  var diem1 = document.getElementById("diemMon1").value * 1;
  var diem2 = document.getElementById("diemMon2").value * 1;
  var diem3 = document.getElementById("diemMon3").value * 1;
  var doiTuong =
    document.getElementById("doiTuong").options[
      document.getElementById("doiTuong").selectedIndex
    ].text;
  var diemDoiTuong = tinhDiemDoiTuong(doiTuong);
  var khuVuc =
    document.getElementById("khuVuc").options[
      document.getElementById("khuVuc").selectedIndex
    ].text;
  var diemKhuVuc = tinhDiemKhuVuc(khuVuc);
  var diemChuan = document.getElementById("diemChuan").value * 1;
  var tongDiem = diem1 + diem2 + diem3 + diemKhuVuc + diemDoiTuong;
  if (diem1 == 0 || diem2 == 0 || diem3 == 0) {
    document.getElementById("ketQua").innerHTML =
      "Bạn đã rớt vì có một môn bị điểm 0";
  } else if (tongDiem >= diemChuan) {
    document.getElementById(
      "ketQua"
    ).innerHTML = `Bạn đã đậu với số điểm là: ${tongDiem}`;
  } else if (tongDiem < diemChuan) {
    document.getElementById("ketQua").innerHTML = "Bạn đã rớt";
  }
}
